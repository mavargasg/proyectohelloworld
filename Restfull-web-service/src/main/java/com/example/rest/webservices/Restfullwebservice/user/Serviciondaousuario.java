package com.example.rest.webservices.Restfullwebservice.user;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Component;

@Component 

public class Serviciondaousuario {
	private static List<usuario> Usuario=new ArrayList<>();
	
	private static int userCount=3;
	
	static {
		Usuario.add(new usuario(1,"Adam",new Date()));
		Usuario.add(new usuario(2,"Eve",new Date()));
		Usuario.add(new usuario(3,"Jack",new Date()));
		
	}
	
	public List<usuario> findAll(){
		return Usuario;
	}
	
	public usuario save(usuario usuario) {
		if(usuario.getId()==null) {
			usuario.setId(++userCount);
			
		}
		Usuario.add(usuario);
		return usuario;
		
	}
	
	public usuario finOne(int id) {
		for(usuario usuario:Usuario) {
			if(usuario.getId()==id) {
				return usuario;
			}
		}
		return null;
	}
	
	public usuario deleteById(int id) {
		Iterator<usuario> iterator= Usuario.iterator();
		while(iterator.hasNext()) {
			usuario usuario = iterator.next();
			if(usuario.getId()==id) {
				iterator.remove();
				return usuario;
			}
		}
		return null;
	}

}
