package com.example.rest.webservices.Restfullwebservice.helloworld;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {
	
	@Autowired
	private MessageSource messagesource;
	
	
	@RequestMapping(path =  "/hello-word")
	public String HelloWorld() {
		return "Hello World";
	}
	
	@RequestMapping(path = "/hello-word-bean")
	public HelloWorldBean HelloWorldBean() {
		return new HelloWorldBean("Hello World");
	}
	
	@RequestMapping(path = "/hello-word/path-variable/{name}")
	public HelloWorldBean HelloWorldpath(@PathVariable String name) {
		return new HelloWorldBean(String.format("Hello World, %s",name));

	}
	
	@RequestMapping(path =  "/hello-word-internalized")
	public String HelloWorldInternationalized(
			//@RequestHeader(name="acept-language",required=false)Locale locale
			) {
		return messagesource.getMessage("good.morning.message", null,"Default Message" 
				//locale
				,LocaleContextHolder.getLocale());
		/*
		return "Hello World";
		*/
	}
	
	
	
	
}
