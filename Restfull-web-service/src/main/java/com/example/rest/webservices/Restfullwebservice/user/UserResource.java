package com.example.rest.webservices.Restfullwebservice.user;

import java.net.URI;
import java.nio.file.attribute.UserPrincipalNotFoundException;
import java.util.List;

import javax.validation.Valid;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
public class UserResource {
	
	@Autowired
	private Serviciondaousuario service;
	
	@GetMapping("/users")
	public List<usuario> retriveAllUsuario(){
		return service.findAll();
	}
	
	@GetMapping("/users/{id}")
	public EntityModel<usuario> retrieveUser(@PathVariable int id) {
		usuario user = service.finOne(id);
		if(user==null) 
			throw new UserNotFoundException("id-"+id);
		
		EntityModel<usuario> model= EntityModel.of(user);
		
		WebMvcLinkBuilder LinktoUsers= linkTo(methodOn(this.getClass()).retriveAllUsuario());
		
		model.add(LinktoUsers.withRel("all-users"));
		
		return model;
		
	}
	
	@PostMapping("/users")
	public ResponseEntity createUser(@Valid @RequestBody usuario usuario) {
		
		service.save(usuario);
		usuario savedUser = service.save(usuario);
		
		URI location = ServletUriComponentsBuilder
				.fromCurrentRequest()
				.path("/{id}")
				.buildAndExpand(savedUser.getId())
				.toUri();
		
		return ResponseEntity.created(location).build();
		
	}
	
	@DeleteMapping("/users/{id}")
	public void DeleteUser(@PathVariable int id) {
		usuario user = service.deleteById(id);
		if(user==null) {
			throw new UserNotFoundException("id-"+id);
		}
		
		
	}

}
